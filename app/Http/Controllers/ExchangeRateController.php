<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ExchangeRateController extends Controller
{
    public function exchangeRate(Request $request)
    {
        $strSource = $request->input('source');
        $strTarget = $request->input('target');
        $floatAmount = (float) str_replace(['$', ','], '', $request->input('amount'));

        $validator = Validator::make($request->all(), [
            'source' => 'required|in:USD,JPY,TWD',
            'target' => 'required|in:USD,JPY,TWD',
            'amount' => 'required|regex:/^\$?[0-9,]+(\.[0-9]{1,2})?$/',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'failed', 'errors' => $validator->errors()->all()], 400);
        }

        $arrExchangeRates = [
            "TWD" => ["TWD" => 1, "JPY" => 3.669, "USD" => 0.03281],
            "JPY" => ["TWD" => 0.26956, "JPY" => 1, "USD" => 0.00885],
            "USD" => ["TWD" => 30.444, "JPY" => 111.801, "USD" => 1]
        ];

        $floatRate = $arrExchangeRates[$strSource][$strTarget];

        return response()->json([
            'msg' => 'success',
            'amount' => '$' . number_format($floatAmount * $floatRate, 2),
        ]);
    }
}

