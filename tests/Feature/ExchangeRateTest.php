<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExchangeRateTest extends TestCase
{
    public function testValidExchange()
    {
        $response = $this->get('/api/exchange-rate?source=USD&target=JPY&amount=$1,525');
        $response->assertStatus(200)
            ->assertJson([
                'msg' => 'success',
            ])->assertJsonStructure([
                'msg',
                'amount',
            ]);
    }

    public function testInvalidInput()
    {
        $response = $this->get('/api/exchange-rate?source=aaa&amount=aaa');
        $response->assertStatus(400)
            ->assertJson([
                'msg' => 'failed',
                'errors' => ['The selected source is invalid.', 'The target field is required.', 'The amount field format is invalid.']
                ]
            );
    }
}
