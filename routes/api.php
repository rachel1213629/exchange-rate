<?php

use App\Http\Controllers\ExchangeRateController;

Route::get('/exchange-rate', [ExchangeRateController::class, 'exchangeRate']);

