
# 換匯ＡＰＩ

提供"USD", "JPY", "TWD"匯率轉換


## API Reference

```http
  GET /api/exchange-rate?source=${source}&target=${target}&amount=${amount}
```

#### 輸入參數
| 參數名稱 | 型態     | 必填     | 描述                |
| :-------- | :------- | :-------| :------------------------- |
| `source` | `string` | 是 |原始貨幣 (e.g., "USD", "JPY", "TWD")｜
| `target` | `string` | 是 |目標貨幣 (e.g., "USD", "JPY", "TWD")｜
| `amount` | `string` | 是 |換匯金額 (數字或貨幣格式 (e.g., "$1,525.45"))


#### 輸出範例
成功回應 (HTTP 狀態碼 200)：

```http
{
    "msg": "success",
    "amount": "$170,496.53"
}
```
失敗回應 (HTTP 狀態碼 400)：：

```http
{
  "msg": "failed",
  "errors": ["錯誤訊息"]
}
```
